'use strict';

/*Ускорение свободного падения
  Не ставить больше 1.5 или изменить predictCollision
  во избежании вечных отскакиваний
*/
const g = 1;

const main = document.querySelector(`.main`);
const staticObjects = [];
const objects = [];

const start_stop_btn = document.querySelector(`.start-stop-btn`);
const clear_btn = document.querySelector(`.clear-btn`);

start_stop_btn.addEventListener('click', start_stop);
clear_btn.addEventListener('click', clear);
main.addEventListener('click', main_clicked);

let interval;
let inGame = false;


function start() {

  interval = setInterval(() => {
    
    gravityOn(objects, g);
    main.innerHTML = '';

    for (let i = 0; i < staticObjects.length; i++) {
      drawObject(staticObjects[i], main);
    }
    for (let i = 0; i < objects.length; i++) {
      drawObject(objects[i], main);
    }

  }, 33);

}

function start_stop() {
  if(inGame) {
    clearInterval(interval);
    inGame = false;
    start_stop_btn.innerHTML = `Start`;
  }
  else {
    /*Добавляем пол, он всегда будет нулевым элементом в данном массиве
      Далее будет добавлена возможность делать произвольные препятствия
    */
    addObject(0, main.offsetHeight - 50 , main, staticObjects, main.offsetWidth, 50, `floor`, `black`);
    start();
    inGame = true;
    start_stop_btn.innerHTML = `Stop`;
  }
}

function clear() {
  objects.length = 0;
  staticObjects.length = 0;
  main.innerHTML = '';
}

/*Находим координаты клика
*/
function main_clicked(e) {
  const relativeCords = setCords(e.pageX, e.pageY, this);
  addObject(relativeCords.x, relativeCords.y, this, objects);
}

/*Передаем координаты относительно документа, 
пересчитываем  координаты относительно 
переданного элемента
*/
function setCords(x, y, area) {
  return {
    x: x - area.offsetLeft,
    y: y - area.offsetTop
  };
}

function createObject(x, y, width = 50, height = 50, 
  type = 'round-obj', color = 'black') {
  return {
    x: x,
    y: y,
    width: width,
    height: height,
    type: type,
    color: color,
    vX: 0, // Начальная скорость
    vY: 0
  };
}

/*Добавляем объект в переданную область
*/
function drawObject(obj, area) {
  area.innerHTML += `
  <div
  class="${obj.type}" 
  style="
    left: ${obj.x}px;
    top: ${obj.y}px;
    width: ${obj.width}px;
    height: ${obj.height}px;
    color: ${obj.color};
    ">
  </div>`;
}

function addObject(x, y, area, arr, 
  width = 50, height = 50, 
  type = 'round-obj', color = 'black') {
  arr.push(createObject(x, y, width, height, type, color));
  drawObject(arr[arr.length - 1], area);
}

/*arr: массив объектов
  g: ускорение свободного падения

  Реализуется гравитация для массива arr
*/
function gravityOn(arr, g) {

  let collide = false;

  for (let item of arr) {

    for (let i = 0; i < staticObjects.length; i++) {
      if (item.y + item.height === staticObjects[i].y &&
          item.vY === 0) {
        continue;
      }
      /*Испраить, убрав в этом случае 3й цикл внутри predictCollision*/
      if (item.y + item.height >= staticObjects[0].y - 1 &&
          item.y + item.height <= staticObjects[0].y + 1 && item.vY <= 3) {
        item.y = staticObjects[i].y - item.height;
        predictCollision(item, staticObjects);
        continue;
      }
    }
    
    collide = isCollision(item, staticObjects);
    predictCollision(item, staticObjects);
    
    if (!collide) {
      item.y += item.vY;
      item.vY += g;
    }
    else if (collide.dir === `top` || collide.dir === `bottom`) {
      objectBound(item, collide.dir);
      item.y += item.vY;
      item.vY += g;
    }
    else if (collide.dir === `left` || collide.dir === `right`) {
      objectBound(item, collide.dir);
      item.x += item.vX;
    }

  }
}

/*objToCheck: объект
  arrStatic: массив объектов, с которыми возможны столкновения

  Функция нужна для того, что бы на большой скорости объект не входил в текстуры
  и при приближению к возможному препятствию корректировалось его положение и скорость
*/
function predictCollision(objToCheck, arrStatic) {
  for (let i = 0; i < arrStatic.length; i++) {

    if (objToCheck.x + objToCheck.width + objToCheck.vX > arrStatic[i].x &&
        objToCheck.x + objToCheck.width - objToCheck.vX <= arrStatic[i].x) {

      correctSpeed(`left`, objToCheck);
      correctPos(`left`, objToCheck, arrStatic[i]);

    }

    if (objToCheck.x + objToCheck.vX < arrStatic[i].x + arrStatic[i].width &&
        objToCheck.x - objToCheck.vX >= arrStatic[i].x + arrStatic[i].width) {

      correctSpeed(`right`, objToCheck);
      correctPos(`right`, objToCheck, arrStatic[i]);

    }

    if (objToCheck.y + objToCheck.height + objToCheck.vY > arrStatic[i].y &&
        objToCheck.y + objToCheck.height - objToCheck.vY <= arrStatic[i].y) {

      correctSpeed(`top`, objToCheck);
      correctPos(`top`, objToCheck, arrStatic[i]);
      
    }

    if (objToCheck.y + objToCheck.vY < arrStatic[i].y + arrStatic[i].height &&
        objToCheck.y - objToCheck.vY >= arrStatic[i].y + arrStatic[i].height) {

      correctSpeed(`bottom`, objToCheck);
      correctPos(`bottom`, objToCheck, arrStatic[i]);

    }

  }

  function correctSpeed(dir, obj) {
    if (dir === `top`) {
      if (obj.vY > 3) {
        obj.vY /= 2;
      }
      else {
        obj.vY = 0;
      }
    }
    else if (dir === `bottom`) {
      obj.vY /= 2;
    }
    else if (dir === `left` || dir === `right`) {
      obj.vX /= 2;
    }
  }

  function correctPos(dir, obj, staticObj) {
    switch (dir) {
      case `top`:
        obj.y = staticObj.y - obj.vY - obj.height;
        break;
      case `bottom`:
        obj.y = staticObj.y + staticObj.height - obj.vY;
        break;
      case `left`:
        obj.x = staticObj.x - obj.vX - obj.width;
        break;
      case `right`:
        obj.x = staticObj.x + staticObj.width - obj.vX;
        break;
      default:
        console.log(`Error in 'correctPos()' switch operator `);
    }
  }

}

/*objToCheck: объект проверяемый на столкновения
  arrStatic: массив объектов с которыми проверяются столкновения
  return: если столкновение было, возвращается объект, .dir: направление 
  столкновения, .number: номер объекта массива с которым было столкновение,
  если столкновения не было, возвразается false.
*/
function isCollision(objToCheck, arrStatic) {
  let numberOfCollideItem = 0;
  for (let i = 0; i < arrStatic.length; i++) {
    if (objToCheck.x + objToCheck.width >= arrStatic[i].x &&
        objToCheck.x <= arrStatic[i].x + arrStatic[i].width &&
        objToCheck.y + objToCheck.height >= arrStatic[i].y &&
        objToCheck.y <= arrStatic[i].y + arrStatic[i].height) {
      numberOfCollideItem = i;
      return {
        dir: getCollideDir(i),
        number: i
      };
    }
  }
  return false;

  function getCollideDir(i) {
    if (objToCheck.x + objToCheck.width >= arrStatic[i].x &&
        objToCheck.x + objToCheck.width - objToCheck.vX <= arrStatic[i].x) {
      return `left`;
    }
    if (objToCheck.x <= arrStatic[i].x + arrStatic[i].width &&
        objToCheck.x - objToCheck.vX >= arrStatic[i].x + arrStatic[i].width) {
      return `right`;
    }
    if (objToCheck.y + objToCheck.height >= arrStatic[i].y &&
        objToCheck.y + objToCheck.height - objToCheck.vY <= arrStatic[i].y) {
      return `top`;
    }
    if (objToCheck.y <= arrStatic[i].y + arrStatic[i].height &&
        objToCheck.y - objToCheck.vY >= arrStatic[i].y + arrStatic[i].height) {
      return `bottom`;
    }
  }
}

/*obj: объект
  dir: направелние, в котором поменяется скорость

  Меняем направление движения
*/
function objectBound(obj, dir) {
  if (dir === `left` || dir === `right`) {
    obj.vX = -obj.vX;
  }
  else if (dir === `top` || dir === `bottom`) {
    obj.vY = -obj.vY;
  }
}